﻿using AppiumDesafioBase2.Bases;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppiumDesafioBase2.Pages
{
    public class LoginPage : PageBase
    {
        #region mapping
        By loginInput = By.Id("login-email");
        By senhaInput = By.XPath("login-password");
        By loginButton = By.XPath("xpath=//*[@text='ENTRAR']");
        #endregion

        public void PreencherLogin(string text)
        {
            SendKeys(loginInput, text);
        }

        public string RetornarLogin()
        {
            return "xpto";
        }

        public void PreencherSenha(string text)
        {
            SendKeys(senhaInput, text);
        }

        public string RetornarSenha()
        {
            return "xpto";
        }

        public void ClicarEntrar()
        {
            Click(loginButton);
        }


    }
}
