﻿using AppiumDesafioBase2.Bases;
using AppiumDesafioBase2.Helpers;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Interfaces;
using OpenQA.Selenium.Appium.PageObjects;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppiumDesafioBase2.Pages
{
    public class ProjectsPage : PageBase
    {

        #region Mapping
        By loginButton = By.XPath("xpath =//*[@text='Login']");
        #endregion

        #region Actions
        public void ClickLogin()
        {
            Click(loginButton);
        }
        #endregion


    }
}
