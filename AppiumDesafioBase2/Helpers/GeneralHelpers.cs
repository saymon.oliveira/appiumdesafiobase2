﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Collections;
using System.Configuration;
using System.IO;
using System.Runtime.CompilerServices;
using System.Diagnostics;



namespace AppiumDesafioBase2.Helpers
{
    public class GeneralHelpers
    {
        public const string ModuloWhatsApp = "WhatsApp";
        public const string ModuloCeres = "Ceres";

        public static string ReturnStringWithRandomCharacters(int size)
        {
            Random random = new Random();

            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            return new string(Enumerable.Repeat(chars, size)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public static string ReturnStringWithRandomNumbers(int size)
        {
            Random random = new Random();

            const string chars = "0123456789";
            return new string(Enumerable.Repeat(chars, size)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public static IEnumerable ReturnCSVData(string csvPath)
        {
            using (StreamReader sr = new StreamReader(csvPath, System.Text.Encoding.GetEncoding(1252)))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    ArrayList result = new ArrayList();
                    result.AddRange(line.Split(';'));
                    yield return result;
                }
            }
        }

        public static int ReturnHourNow()
        {
            DateTime date = DateTime.Now;
            return date.Hour;
        }

        public static string ReturnProjectPath()
        {
            string pth = System.Reflection.Assembly.GetCallingAssembly().CodeBase;

            string actualPath = pth.Substring(0, pth.LastIndexOf("bin"));

            return new Uri(actualPath).LocalPath;
        }

        public static string GetScreenshot(string path)
        {
            string testName = TestContext.CurrentContext.Test.MethodName;
            string date = DateTime.Now.ToString().Replace("/", "_").Replace(":", "_").Replace(" ", "-");

            Screenshot screenShot = ((ITakesScreenshot)DriverFactory.INSTANCE).GetScreenshot();
            string filePathAndName = path + "/" + testName + "_" + date + ".png";
            screenShot.SaveAsFile(filePathAndName, ScreenshotImageFormat.Png);

            return filePathAndName;
        }

        public static string GetProjectPath()
        {
            string pth = System.Reflection.Assembly.GetCallingAssembly().CodeBase;

            string actualPath = pth.Substring(0, pth.LastIndexOf("bin"));

            return new Uri(actualPath).LocalPath;
        }

        [MethodImpl(MethodImplOptions.NoInlining)]
        public static string GetMethodNameByLevel(int level)
        {
            StackTrace st = new StackTrace();
            StackFrame sf = st.GetFrame(level);
            return sf.GetMethod().Name;
        }

        public static string GetRandomIDNumber()
        {
            return Guid.NewGuid().ToString();
        }

        public static void EnsureDirectoryExists(string fullReportFilePath)
        {
            var directory = Path.GetDirectoryName(fullReportFilePath);
            if (!Directory.Exists(directory)) Directory.CreateDirectory(directory);
        }

        public static void RetornarURLSistema()
        {
            ExtentReportHelpers.AddTestInfo(2, "https://" +
                                                ConfigurationManager.AppSettings["base_homol"] +
                                                ConfigurationManager.AppSettings["environment_url"]);
        }

        public static string SomarValorEmString(string valorOriginal, double incremento)
        {
            return (Convert.ToDouble(valorOriginal) + incremento).ToString();         
        }

        public static string ReturnHourNowAndAddMinutes(int minutesAdd)
        {
            DateTime currentTime = TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.FindSystemTimeZoneById("E. South America Standard Time"));
            return currentTime.AddMinutes(minutesAdd).ToString("HH:mm");
        }

        public static void AguardarHoraMinutoExpirar(string originalTime)
        {
            DateTime timeTest = DateTime.Parse(originalTime);
            int value = 0;
            
            while (value >= 0)
            {
                DateTime currentTime = TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.FindSystemTimeZoneById("E. South America Standard Time"));
                currentTime.ToString("HH:mm");
                value = DateTime.Compare(timeTest, currentTime);
            }

        }

        public static string ConverterDataPadraoResumoReserva(string data)
        {
            //       06/09/2019
            //      06 Setembro
            var dataParticionada = data.Split('/');
            return dataParticionada[0] + " " + converteMes(Convert.ToInt32(dataParticionada[1]));
        }


        public static string converteMes(int mes)
        {
            string mesConvertido = "";
            switch (mes)
            {
                case 01:
                mesConvertido = "Janeiro";
                break;
                case 02:
                mesConvertido = "Fevereiro";
                break;
                case 03:
                mesConvertido = "Março";
                break;
                case 04:
                mesConvertido = "Abril";
                break;
                case 05:
                mesConvertido = "Maio";
                break;
                case 06:
                mesConvertido = "Junho";
                break;
                case 07:
                mesConvertido = "Julho";
                break;
                case 08:
                mesConvertido = "Agosto";
                break;
                case 09:
                mesConvertido = "Setembro";
                break;
                case 10:
                mesConvertido = "Outubro";
                break;
                case 11:
                mesConvertido = "Novembro";
                break;
                case 12:
                mesConvertido = "Dezembro";
                break;
                default:
                mesConvertido = "Janeiro";
                break;
            }
            return mesConvertido;
        }




    }//fim class
}//fim namespace
