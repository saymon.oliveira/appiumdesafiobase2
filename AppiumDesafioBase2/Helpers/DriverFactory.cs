﻿using OpenQA.Selenium;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Android;
using OpenQA.Selenium.Appium.Enums;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Edge;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppiumDesafioBase2.Helpers
{   
    public class DriverFactory  
    {
        public static AppiumDriver<AndroidElement> INSTANCE { get; set; } = null;
        public WebDriverWait wait;

        public static void CreateInstance()
        {
            string plataforma = ConfigurationManager.AppSettings["plataforma"].ToString();
            bool apk = bool.Parse(ConfigurationManager.AppSettings["apk"].ToString());

            if (INSTANCE == null)
            {
                switch (plataforma)
                {
                    case "android":
                        var options = new AppiumOptions();

                        if (apk)
                        {
                            options.AddAdditionalCapability(MobileCapabilityType.App, GeneralHelpers.GetProjectPath() + "/Apk/" + ConfigurationManager.AppSettings["app"].ToString());
                        }
                        else
                        {
                            options.AddAdditionalCapability(MobileCapabilityType.Udid, ConfigurationManager.AppSettings["udid"].ToString());
                            options.AddAdditionalCapability(AndroidMobileCapabilityType.AppPackage, ConfigurationManager.AppSettings["appPackage"].ToString());
                            options.AddAdditionalCapability(AndroidMobileCapabilityType.AppActivity, ConfigurationManager.AppSettings["appActivity"].ToString());
                        }

                        INSTANCE = new AndroidDriver<AndroidElement>(new Uri(ConfigurationManager.AppSettings["url_appium"].ToString()), options, TimeSpan.FromMinutes(1));

                        break;

                    case "ios":
                        
                        break;

                    default:
                        throw new Exception("A plataforma informada não existe ou não é suportada pela automação");
                }
            }
        }

        public static void QuitInstace()
        {
            INSTANCE.Quit();
            INSTANCE.Dispose();
            INSTANCE = null;
        }

    }
}
