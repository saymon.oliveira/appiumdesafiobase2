﻿using AppiumDesafioBase2.Bases;
using AppiumDesafioBase2.Helpers;
using AppiumDesafioBase2.Pages;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium.Android;
using OpenQA.Selenium.Appium.Enums;
using OpenQA.Selenium.Remote;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AppiumDesafioBase2.Tests
{
    class Test : TestBaseAndroid
    {

        ProjectsPage projects;
        LoginPage login;

        [Test()]
        public void TestUntitled()
        {
            projects = new ProjectsPage();
            login = new LoginPage();
            Thread.Sleep(6000);
            projects.ClickLogin();

            login.PreencherLogin("loginfake");
            login.PreencherSenha("senhafake");
            login.ClicarEntrar();
         }


    }
}
