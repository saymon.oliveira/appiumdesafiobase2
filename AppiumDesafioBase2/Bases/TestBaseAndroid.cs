﻿using AppiumDesafioBase2.Helpers;
using NUnit.Framework;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Android;
using OpenQA.Selenium.Appium.Enums;
using OpenQA.Selenium.Appium.Service;
using OpenQA.Selenium.Appium.Service.Options;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Remote;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppiumDesafioBase2.Bases
{
    public class TestBaseAndroid
    {
        private AppiumLocalService service;
        private AppiumServiceBuilder builder;

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {

            Environment.SetEnvironmentVariable(AppiumServiceConstants.NodeBinaryPath, @"C:\Program Files\nodejs\node.exe");
            //Environment.SetEnvironmentVariable(AppiumServiceConstants.AppiumBinaryPath, @"C:\Program Files\Appium\resources\app\node_modules\appium\lib\main.js");
            //ExtentReportHelpers.CreateReport();
            builder = new AppiumServiceBuilder();
            service = builder.Build();
            service.Start();
        }

        [SetUp]
        public void SetUp()
        {
            DriverFactory.CreateInstance();
        }

        [TearDown]
        public void afterTest()
        {
            DriverFactory.INSTANCE.CloseApp();
        }

        //[OneTimeTearDown]
        //public void afterSuite()
        //{
        //    ExtentReportUtils.generateReport();
        //    service.stop();
        //}

    }
}
