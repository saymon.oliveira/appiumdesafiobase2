﻿using AppiumDesafioBase2.Helpers;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Android;
using OpenQA.Selenium.Appium.Interfaces;
using OpenQA.Selenium.Appium.iOS;
using OpenQA.Selenium.Appium.PageObjects;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppiumDesafioBase2.Bases
{
    public class PageBase
    {
        //Variaveis globais
        protected WebDriverWait wait { get; private set; }
        protected AppiumDriver<AndroidElement> driverAndroid = null;
        protected AppiumDriver<IOSElement> driverIOS = null;
        //protected AppiumDriver<AppiumWebElement> driver;
        protected IJavaScriptExecutor javaScriptExecutor { get; private set; }

        //Construtor
        public PageBase()
        {            
            wait = new WebDriverWait(DriverFactory.INSTANCE, TimeSpan.FromSeconds(Convert.ToDouble(ConfigurationManager.AppSettings["timeout_default"].ToString())));
            driverAndroid = DriverFactory.INSTANCE;
            javaScriptExecutor = (IJavaScriptExecutor)DriverFactory.INSTANCE;
        }

        protected AppiumWebElement WaitForAndroidElement(By locator)
        {
            wait.Until(ExpectedConditions.ElementExists(locator));
            AndroidElement element = DriverFactory.INSTANCE.FindElement(locator);
            wait.Until(ExpectedConditions.ElementToBeClickable(element));
            return element;
        }

        //protected IWebElement WaitForIOSElement(By locator)
        //{
        //    wait.Until(ExpectedConditions.ElementExists(locator));
        //    IOSElement element = driver.FindElement(locator);
        //    wait.Until(ExpectedConditions.ElementToBeClickable(element));
        //    return element;
        //}

        protected void WaitForElementToBeClickable(AndroidElement element)
        {
            wait.Until(ExpectedConditions.ElementToBeClickable(element));
        }
        protected void WaitForElementByTime(AndroidElement element, TimeSpan time)
        {
            WebDriverWait waitTime = new WebDriverWait(driverAndroid, time);
           // waitTime.Until(ExpectedConditions.ElementIsVisible(element));
        }

        protected void Click(By locator)
        {
            Stopwatch timeOut = new Stopwatch();
            timeOut.Start();

            while (timeOut.Elapsed.Seconds <= Convert.ToInt32(ConfigurationManager.AppSettings["timeout_default"].ToString()))
            {
                try
                {
                    WaitForAndroidElement(locator).Click();
                    timeOut.Stop();
                    //ExtentReportHelpers.AddTestInfo(3, "");
                    return;
                }
                catch (System.Reflection.TargetInvocationException)
                {

                }
                catch (StaleElementReferenceException)
                {

                }
                catch (System.InvalidOperationException)
                {

                }
                catch (WebDriverException e)
                {
                    if (e.Message.Contains("Other element would receive the click"))
                    {
                        continue;
                    }

                    if (e.Message.Contains("Element is not clickable at point"))
                    {
                        continue;
                    }

                    throw e;
                }
            }
        }



        protected void SendKeys(By locator, String text)
        {
            WaitForAndroidElement(locator).SendKeys(text);
            //ExtentReportUtils.addTestInfo(3, "PARAMETER: " + text);
        }

        //protected void sendKeysWithoutWaitVisible(MobileElement element, String text)
        //{
        //    element.sendKeys(text);
        //    ExtentReportUtils.addTestInfo(3, "PARAMETER: " + text);
        //}
        //protected void clear(MobileElement element)
        //{
        //    waitForElement(element);
        //    element.clear();
        //}
        //protected void clearAndSendKeys(MobileElement element, String text)
        //{
        //    waitForElement(element);
        //    element.clear();
        //    element.sendKeys(text);
        //}
        //protected void clearAndSendKeysAlternative(MobileElement element, String text)
        //{
        //    waitForElement(element);
        //    element.sendKeys(Keys.CONTROL + "a");
        //    element.sendKeys(Keys.DELETE);
        //    element.sendKeys(text);
        //}
        //protected String getText(MobileElement element)
        //{
        //    waitForElement(element);
        //    String text = element.getText();
        //    ExtentReportUtils.addTestInfo(3, "RETURN: " + text);
        //    return text;
        //}
        //protected String getValue(MobileElement element)
        //{
        //    waitForElement(element);
        //    String text = element.getAttribute("value");
        //    ExtentReportUtils.addTestInfo(3, "RETURN: " + text);
        //    return text;
        //}
        //protected boolean returnIfElementIsDisplayed(MobileElement element)
        //{
        //    waitForElement(element);
        //    boolean result = element.isDisplayed();
        //    ExtentReportUtils.addTestInfo(3, "RETURN: " + result);
        //    return result;
        //}
        //protected boolean returnIfElementIsEnabled(MobileElement element)
        //{
        //    waitForElement(element);
        //    boolean result = element.isEnabled();
        //    ExtentReportUtils.addTestInfo(3, "RETURN: " + result);
        //    return result;
        //}
        //protected boolean returnIfElementIsSelected(MobileElement element)
        //{
        //    waitForElement(element);
        //    boolean result = element.isSelected();
        //    ExtentReportUtils.addTestInfo(3, "RETURN: " + result);
        //    return result;
        //}
        //protected void scrollUsingTouchActions_ByElements(MobileElement startElement, MobileElement endElement, int seconds)
        //{
        //    TouchAction actions = new TouchAction(driver);
        //    actions.press(PointOption.point(startElement.getLocation().x, startElement.getLocation().y))
        //            .waitAction(WaitOptions.waitOptions(Duration.ofSeconds(seconds)))
        //            .moveTo(PointOption.point(endElement.getLocation().x, endElement.getLocation().y)).release().perform();
        //}
        //protected void scrollUsingTouchActions(int startX, int startY, int endX, int endY, int seconds)
        //{
        //    TouchAction actions = new TouchAction(driver);
        //    actions.press(PointOption.point(startX, startY))
        //            .waitAction(WaitOptions.waitOptions(Duration.ofSeconds(seconds)))
        //            .moveTo(PointOption.point(endX, endY)).release().perform();
        //}
        //protected void longPress(MobileElement element)
        //{
        //    waitForElement(element);
        //    TouchActions action = new TouchActions(driver);
        //    action.longPress(element);
        //    action.perform();
        //}
        //protected void scrolling(String direction)
        //{
        //    JavascriptExecutor js = (JavascriptExecutor)driver;
        //    HashMap<String, String> scrollObject = new HashMap<String, String>();
        //    scrollObject.put("direction", direction);
        //    js.executeScript("mobile: scroll", scrollObject);
        //}
        //protected void tap(MobileElement element)
        //{
        //    waitForElement(element);
        //    TouchActions action = new TouchActions(driver);
        //    action.singleTap(element);
        //    action.perform();
        //}
        //protected void doubleTap(MobileElement element)
        //{
        //    waitForElement(element);
        //    TouchActions action = new TouchActions(driver);
        //    action.doubleTap(element);
        //    action.perform();
        //}

        ////Função para realizar scroll somente em Android
        //protected MobileElement scrollToElementAndroid(String string)
        //{
        //    return ((AndroidDriver<MobileElement>)driver).findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textContains(\"" + string + "\").instance(0))");
        //}





    }
    }
